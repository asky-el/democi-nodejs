const port = process.env.PORT || 3000
const server = require('./src/server/api');

process.on('uncaughtException', ()=> {
  logger.error(err);
  process.exit(1);
});

server.listen(port, (err) => {
  if (err) throw err;
  console.log(`Listening on http://localhost:${port}`)
});
