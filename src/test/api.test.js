const test = require('tape');
const request = require('supertest');

const service = require('../server/api');

test('GET /status', (t) => {
  request(service)
    .get('/status')
    .expect(200)
    .end((err,res) => {
      t.error(err)
      t.equal(res.text, ' APIs WORKING!')
      t.end();
    })
})

test('GET /demo', (t) => {
  request(service)
    .get('/demo')
    .expect(200)
    .end((err,res) => {
      t.error(err)
      t.deepEqual(res.body, {'demo': 'gitlab CI berhasil,tinggal deploy ke server ESD'})
      t.end();
    })
})
