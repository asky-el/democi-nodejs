const express = require('express');
const bodyParser = require('body-parser');
const routes = require('../routes/index.js');
const service = express();

service.use(bodyParser.json());

service.get('/', (req, res) => {
  console.log('Asky said : homepage is OK!')
})

service.get('/status', (req, res) => {
  console.log('Checking Demo API....')
  res.status(200).send('Asky said : Demo API OK!')
});

service.get('/demo', routes);

exports = module.exports = service;
